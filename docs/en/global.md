### Filling parameters from context

You can fill in parameters from context, formed from results of methods' responses in same request. It can be done by defining a link like a `&<index>.key1.key2.[index1].key3`

*The index must also be separated by dots*

Example of request:

```json
[
    {
        "method": "user.list",
        "params": {
            "status": "active"
        }
    },

        // Example response of first method (user.list)
        {
            "status": 1,
            "data": {
                "ids": [
                    1, 3, 5, 65
                ]
            }
        }


    {
        "method": "user.detail",
        "params": {
            "id": "&0.ids.[3]"
        }
    }

        // Result of filling params of second method from context (before params validation)
        {
            "id": 5
        }
]
```
